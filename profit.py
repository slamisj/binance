#!/usr/bin/env python3
import argparse

import pytz
from binance import Client
import os
from datetime import datetime, timedelta

parser = argparse.ArgumentParser(description='List bot profits in the last 10 days, based on take profit settings and sum of sells.')
parser.add_argument("pairs", help="Comma-separate traded pairs. "
                                  "Use slash to specify other than 0.9 %% default profit value. "
                                  "e.g. MATICBUSD/2,NEARBUSD - MATIC 2 %%, NEAR 0.9 %%.")
parser.add_argument("-dp", "--defaultProfit", type=float, default=0.9, help="Default take profit to use.")
parser.add_argument("-r", "--rate", type=float, default=22, help="Rate USD CZK.")
parser.add_argument("-v", "--verbose", action="store_true")

args = parser.parse_args()

BINANCE_API_KEY = os.getenv('BINANCE_API_KEY')
BINANCE_API_SECRET = os.getenv('BINANCE_API_SECRET')

client = Client(BINANCE_API_KEY, BINANCE_API_SECRET)

totalBuys = totalSells = 0
totalSellAmount = totalBuyAmount = 0.0

pairs = args.pairs.split(",")


now = datetime.now()
startTime = datetime.now() - timedelta(days=10)
profits = {}
profitsByPair = {}
pairKeys = []

if args.verbose:
    print(now.strftime("%H:%M:%S"))
    print("===")
    print(f"PAIR: # B=numBuys / S=numSells | $ B=buyAmount / S=sellAmount")
for pairData in pairs:
    parts = pairData.split("/")
    pair = parts[0]
    pairKeys.append(pair)
    tp = args.defaultProfit
    if len(parts) == 2:
        tp = float(parts[1])

    buys = sells = 0
    sellAmount = buyAmount = 0.0

    data = client.get_my_trades(symbol=pair,startTime=round(startTime.timestamp() * 1000),limit=1000)
    for trade in data:
        time = datetime.fromtimestamp(trade["time"] / 1000.0, pytz.timezone("Europe/Prague")).strftime("%Y-%m-%d")

        if trade["isBuyer"]:
            buys += 1
            buyAmount += float(trade["quoteQty"])
        else:
            sells += 1
            sellAmount += float(trade["quoteQty"])
            if not time in profits:
                profits[time] = 0.0
            if not pair in profitsByPair:
                profitsByPair[pair] = {}
            if not time in profitsByPair[pair]:
                profitsByPair[pair][time] = 0.0
            toAdd = float(trade["quoteQty"]) / (100 + tp) * tp * args.rate
            profits[time] += toAdd
            profitsByPair[pair][time] += toAdd

    if args.verbose:
        print(f"{pair}: # B={buys} / S={sells} | $ B={round(buyAmount)} / S={round(sellAmount)}")
    totalBuys += buys
    totalSells += sells
    totalSellAmount += sellAmount
    totalBuyAmount += buyAmount

if args.verbose:
    print("===")
    print(f"Total: # B={totalBuys} / S={totalSells} | $ B={round(totalBuyAmount)} / S={round(totalSellAmount)}")
    print("===")

print("day\tprofit CZK")
for k, v in sorted(profits.items()):
    pairData = ""
    for pairKey in pairKeys:
        if pairKey in profitsByPair and k in profitsByPair[pairKey]:
            pairData = f"{pairData} {pairKey}: {round(profitsByPair[pairKey][k])}"
    print(f"{k}\t{round(v)} -{pairData}")
