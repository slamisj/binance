#!/usr/bin/env python3
import argparse

from binance import Client
from datetime import datetime

client = Client()

parser = argparse.ArgumentParser(description="Show the max loss for the given pairs and period.")
parser.add_argument("pairs", help="Comma-separate traded pairs, e.g. ETHUSDT,DOTUSDT.")
parser.add_argument("-d", "--days", type=int, default=180, help="Number of days, default 180.")

args = parser.parse_args()

pairs = args.pairs.split(",")
days = args.days

for pair in pairs:
    klines = client.get_historical_klines(pair, Client.KLINE_INTERVAL_1DAY, str(days) + " day ago UTC")
    min = 1000000
    max = 0
    maxFall = 0
    minTime = maxTime = datetime.utcnow()
    out = ""

    for kline in reversed(klines):
        timestamp = kline[0]
        high = float(kline[2])
        low = float(kline[3])
        time = datetime.fromtimestamp(timestamp/1000.0).strftime("%d.%m.%Y")

        if low < min:
            min = low
            minTime = time

        currentFall = round(100 - (min / high) * 100)
        if currentFall > maxFall:
            maxFall = currentFall
            max = high
            maxTime = time
            out = f"{pair}\t{maxFall}\t from {max} {maxTime} to {min} {minTime}"

    print(out)
