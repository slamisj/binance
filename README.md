# Binance

Set of python scripts to show various TradeSanta + Binance data. Run `command -h` (e.g. `bot -h`) to see the accepted parameters.

# Features

## bot
Simulate the grid bot behavior on the given pair, optionally showing graph of buys/sells.
```
bot ETHBUSD,AVAXBUSD --profit 2 --extraOrder 2 --fall 60 --daysFrom 7
```

| PAIR-TP-EO | #buys | #sells | depth | cycles | price USD | profit USD | month profit USD | profit CZK | month profit CZK | profit % |
|---|---|---|---|---|---|---|---|---|---|---|
| ETHBUSD-2.0-2.0 | 52 | 43 | 13 | 0 | 330 | 9 | 39 | 198 | 858 | 11.818 |
| AVAXBUSD-2.0-2.0 | 69 | 60 | 16 | 0 | 330 | 13 | 56 | 286 | 1232 | 16.97 |

Notes:
* Only grid long bots are supported.
* Accepted fall in percent is used only to calculate bot price, it does not prevent virtual bot from trading.
* Dollar-like coins are expected as quote currency (BUSD, USDT).
* Use REAL profit for take profit parameter. E.g. if you use BUSD (no fee on Binance) and set Santa bot with 0.7 % TP, enter take profit 0.9 % TP, as Santa automatically adds 0.2 % to cover fees.


## profit
List bot profits in the last 10 days, based on take profit settings and sum of sells.
```
profit LUNABUSD/0.7,NEARBUSD/2
```
```
day	        profit CZK
2022-01-18	5
2022-01-19	10
2022-01-20	8
2022-01-21	6
2022-01-22	15
```

Notes:
* Dollar-like coins are expected as quote currency (BUSD, USDT).
* Use REAL profit for take profit parameter. E.g. if you use BUSD (no fee on Binance) and set Santa bot with 0.7 % TP, enter take profit 0.9 % TP, as Santa automatically adds 0.2 % to cover fees.
* Script uses Binance API and doesn't know about Santa bots. So if you do manual trades or run multiple bots on the same pair with different setting, results won't be accurate.

# fall
Show the max loss for the given pairs and period, i.e. the greatest difference between high price and following low price.
```
fall LUNABUSD,NEARBUSD --days 360
```
```
LUNABUSD	82 %	 from 21.9914 22.03.2021 to 3.92 23.05.2021
NEARBUSD	80 %	 from 7.5916 13.03.2021 to 1.5487 20.07.2021
```

# Install

1. Install python, pip and pipenv (https://www.python.org/, `pip install pipenv`)
2. Clone or download this repository
3. Open this directory in terminal
4. Update dependencies `pipenv update`
5. Setup environment variables `BINANCE_API_KEY`, `BINANCE_API_SECRET`. Needed only for `profit` script.
   * Mac - `export BINANCE_API_KEY=xxx; export BINANCE_API_SECRET=yyy`
   * Windows - `set BINANCE_API_KEY=xxx; set BINANCE_API_SECRET=yyy`
6. Run the desired script `bot -h`
7. Optionally, add this directory to PATH to run it from any directory
   * Mac - `export PATH=$PATH:/Users/me/binance`
   * Windows - `set PATH=%PATH%;C:\your\path\here\`

