#!/usr/bin/env python3
import argparse

from binance import Client
from datetime import datetime
import plotly.graph_objects as go


def info(s):
    if args.verbose:
        print("* " + s)


def format_price(s):
    return round(s, 3)


def print_kline(time, low, high):
    if args.verbose:
        print(f"{time}: L {low} H {high}")


client = Client()

parser = argparse.ArgumentParser(description="Simulate the grid bot behavior on the given pair, optionally showing graph of buys/sells.")
parser.add_argument("pairs", help="Comma-separate traded pairs, e.g. ETHUSDT,DOTUSDT.")
parser.add_argument("-tp", "--profit", default="0.9", help="Real take profit in percent, default 0.9.")
parser.add_argument("-eo", "--extraOrder", default="0.7", help="Extra order in percent, default 0.7.")
parser.add_argument("-f", "--fall", type=int, default=60, help="Allowed fall in percent, default 60.")
parser.add_argument("-df", "--daysFrom", type=int, default=30, help="Offset in days to start from, default 30.")
parser.add_argument("-dt", "--daysTo", type=int, default=0, help="Offset in days to finish, default 0.")
parser.add_argument("--graph", action="store_true", help="Show graph.")
parser.add_argument("-v", "--verbose", action="store_true")
parser.add_argument("-r", "--rate", type=float, default=22, help="Rate USD CZK.")
parser.add_argument("-ov", "--orderVolume", type=float, default=11, help="Order volume.")

args = parser.parse_args()

pairs = args.pairs.split(",")
profitRates = args.profit.split(",")
eoRates = args.extraOrder.split(",")

maxFall = args.fall
daysOffsetFrom = args.daysFrom
daysOffsetTo = args.daysTo

numDays = daysOffsetFrom - daysOffsetTo

volume = args.orderVolume
usdRate = args.rate
DAYS_PER_MONTH = 30

print(f"PAIR-TP-EO\t#buys\t#sells\tdepth\tcycles\tprice USD\tprofit USD\tmonth profit USD\tprofit CZK\tmonth profit CZK\tprofit %")
for pair in pairs:
    for idx, profitRateInput in enumerate(profitRates):
        tpPercent = float(profitRateInput.replace("%", ""))
        profitRate = tpPercent / 100
        eoPercent = float(eoRates[idx].replace("%", ""))
        eoRate = eoPercent / 100

        numBuys = numSells = maxDepth = numCycles = 0

        if args.verbose:
            print("===")
            print(pair)
            print("===")

        sellValues = []

        klines = client.get_historical_klines(pair, Client.KLINE_INTERVAL_1MINUTE, str(daysOffsetFrom) + " days ago UTC", str(daysOffsetTo) + " days ago UTC")
        #klines = client.get_historical_klines(pair, Client.KLINE_INTERVAL_1MINUTE, "2022-02-06T00:24:00.000+00:00", "2022-02-09T00:03:00.000+00:00")
        startingBuyValue = buyValue = sellValue = buyStep = 0.0

        xs = []
        avgs = []
        buysXs = []
        buysYs = []
        newCycleBuysXs = []
        newCycleBuysYs = []
        sellsXs = []
        sellsYs = []
        lows = []
        highs = []

        for idx, kline in enumerate(klines):
            timestamp = kline[0]
            open = float(kline[1])
            high = float(kline[2])
            low = float(kline[3])
            close = float(kline[4])
            time = datetime.fromtimestamp(timestamp/1000.0).strftime("%d.%m. %H:%M:%S")

            xs.append(time)
            avgs.append((open + close) / 2)
            lows.append(low)
            highs.append(high)

            if numBuys == numSells or low <= buyValue:
                print_kline(time, low, high)
                if numBuys == numSells:
                    info(f"Starting a new cycle")
                    numCycles += 1
                    if numBuys == 0:
                        startingBuyValue = open
                    else:
                        startingBuyValue = sellValue
                    buyStep = startingBuyValue * eoRate
                    buyValue = startingBuyValue
                    newCycleBuysXs.append(time)
                    newCycleBuysYs.append(buyValue)

                buysXs.append(time)
                buysYs.append(buyValue)

                numBuys += 1

                # B1, B2, waiting for S2 (sellValue) or B3 (buyValue). B3 happens.
                # So now we want to buy at B4, or sell at S3

                newBuyValue = buyValue - buyStep
                newSellValue = buyValue * (1 + profitRate)
                info(f"Buy for {format_price(buyValue)}, BO {format_price(newBuyValue)}, SO {format_price(newSellValue)}")

                sellValue = newSellValue
                buyValue = newBuyValue
            elif high >= sellValue:
                numSells += 1
                print_kline(time, low, high)
                if numSells == numBuys:
                    info(f"Sell last order for {format_price(sellValue)}")
                    sellsXs.append(time)
                    sellsYs.append(sellValue)
                else:
                    # B1, B2, waiting for S2 (sellValue) or B3 (buyValue). S2 happens, gaining from B2 price.
                    # So now we want to buy at B2 again, or sell at S1

                    # B2
                    newBuyValue = buyValue + buyStep
                    # S1
                    newSellValue = (buyValue + 2 * buyStep) * (1 + profitRate)
                    info(f"Sell for {format_price(sellValue)}, BO {format_price(newBuyValue)}, SO {format_price(newSellValue)}")
                    sellsXs.append(time)
                    sellsYs.append(sellValue)
                    sellValue = newSellValue
                    buyValue = newBuyValue

            if numBuys - numSells > maxDepth:
                maxDepth = numBuys - numSells

        price = round(maxFall / eoPercent * volume)
        profit = round(profitRate * numSells * volume)
        monthProfit = round(profit * DAYS_PER_MONTH / numDays)
        profitRate = monthProfit / price * 100
        monthProfitCzk = monthProfit * usdRate
        profitCzk = profit * usdRate
        #f"PAIR-TP-EO\t#buys\t#sells\tdepth\tcycles\tprice USD\tprofit USD\tmonth profit USD\tprofitCZK\tmonth profit CZK\tprofit %"
        print(f"{pair}-{tpPercent}-{eoPercent}\t{numBuys}\t{numSells}\t{maxDepth}\t{numCycles-1}\t{price}\t{profit}\t{monthProfit}\t{profitCzk}\t{monthProfitCzk}\t{format_price(profitRate)}")

        if args.graph:
            title = f"{pair} {numDays} days, EO {eoRate * 100}%, max depth {maxDepth}, {numCycles} cycles, # B={numBuys} / S={numSells}"

            fig = go.Figure()
            fig.add_trace(go.Scatter(x=xs, y=avgs,
                                mode='lines',
                                name='avg'))
            fig.add_trace(go.Scatter(x=buysXs, y=buysYs,
                                mode='markers', name='buys'))
            fig.add_trace(go.Scatter(x=newCycleBuysXs, y=newCycleBuysYs,
                                mode='markers', name='new cycle buys'))
            fig.add_trace(go.Scatter(x=sellsXs, y=sellsYs,
                                mode='markers', name='sells'))
            fig.add_trace(go.Scatter(x=xs, y=lows,
                                mode='lines',
                                name='low', visible='legendonly'))
            fig.add_trace(go.Scatter(x=xs, y=highs,
                                mode='lines',
                                name='high', visible='legendonly'))

            fig.update_layout(title=title,
                               xaxis_title='Time',
                               yaxis_title='Price')

            fig.write_html(f"output-{pair}-{tpPercent}-{eoPercent}.html", auto_open=True)
